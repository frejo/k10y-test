FROM ubuntu:latest

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

# Install python
RUN apt-get update && \
    apt-get -y install --no-install-recommends \
        python3 python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install kubernetes

# Copy the current folder which contains source code to the Docker image under /usr/src
COPY app.py /app.py

# Run the application
CMD [ "python3", "/app.py" ]
