from kubernetes import client, config
from kubernetes.client.api import core_v1_api
from threading import Thread
import socket
from time import sleep
import logging
import signal

# Set up GraceFullKiller class to handle SIGTERM
class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self, *args):
    self.kill_now = True

KILLER = GracefulKiller()

# get pod name and IP
pod_name = socket.gethostname()
pod_ip = socket.gethostbyname(pod_name)

# Configure logger - add pod name to log messages
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(pod_name)s] %(message)s')

old_factory = logging.getLogRecordFactory()

def record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.pod_name = pod_name
    return record

logging.setLogRecordFactory(record_factory)


# Configure API to use Pod's ServiceAccount
config.load_incluster_config()

# get namespace
with open('/var/run/secrets/kubernetes.io/serviceaccount/namespace', 'r') as f:
    namespace = f.read()


v1 = core_v1_api.CoreV1Api()

# Listening thread
def ping_listen():
    # upen UPD socket to listen for incoming connections
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((pod_ip, 5000))
    sock.settimeout(1)
    logging.info(f"Listening on {pod_ip}:5000")

    while not KILLER.kill_now:
        try:
            data, addr = sock.recvfrom(1024)
            logging.info(f"received message: {data} from {addr}")
        except socket.timeout:
            pass

def ping_send(addr):
    logging.info(f"Sending ping to {addr}")
    # open UDP socket to send pings
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(b"ping", (addr, 5000))

def main():
    logging.info("Listing other pods with their IPs:")

    ret = v1.list_namespaced_pod(namespace, watch=False)
    for i in ret.items:
        if i.metadata.name == pod_name:
            continue
        logging.info(f"{i.status.pod_ip}, {i.metadata.namespace}, {i.metadata.name}")
        ping_send(i.status.pod_ip)
    
    # Set label
    v1.patch_namespaced_pod(pod_name, namespace, {"metadata": {"labels": {"mylabel": "true"}}})


if __name__ == '__main__':
    t = Thread(target=ping_listen)
    t.start()
    sleep(4)
    main()
    t.join()