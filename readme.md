# K10y:test (KubernetesPy)

```sh
# Start minikube
minikube start
# Build the docker image inside minikube
minikube image build -t k10y:test .
# Setup roles to allow pods to access API
minikube kubectl -- apply -f ClusterRole.yaml
minikube kubectl -- apply -f ClusterRoleBinding.yaml
# Deploy the app
minikube kubectl -- apply -f deploy.yaml
# See the action in log
minikube kubectl -- logs -l app=k10y
# check that the label "mylabel" has been set from inside the pod
minikube kubectl -- get pods --show-labels
```